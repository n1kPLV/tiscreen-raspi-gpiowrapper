# TIScreen GPIO-Wrapper for RaspberryPi

This is just a small python script to automatically print a screenshot of a connected TI-Nspire CX (possibly also others. Feel free to test) to the default printer when a button is pressed (e.g. a GPIO-Pin of a Raspberry Pi is either pulled high or low).

## Requirements
This Software depends on [Python > 3.5](https://www.python.org/downloads/), [RPi.GPIO](https://pypi.org/project/RPi.GPIO/) and [TIScreen](https://gitlab.com/n1kPLV/TIScreen)
