#!/usr/bin/env python3.5

import RPi.GPIO as GPIO
import time as time
import subprocess as subprocess
GPIO.setmode(GPIO.BCM)

#Variables
LED_Ready = 13
LED_Error = 12
BTN_Print = 21
TIPrintPath = "../TIScreen/src/TIScreen"
input = "/tmp/unprocessed.png"
processed = "/tmp/big.png"

# init code
GPIO.setup(BTN_Print, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(LED_Error, GPIO.OUT)
GPIO.setup(LED_Ready, GPIO.OUT)
pwm = GPIO.PWM(LED_Ready, 0.5) 

try:
    pwm.start(0)
    while 1:
        pwm.ChangeDutyCycle(100)
		#stop execution until button is pressed
        GPIO.wait_for_edge(BTN_Print, GPIO.FALLING) 
        pwm.ChangeDutyCycle(50)
		#Simple Debugging
        print("Button pressed!")
        #clean everything up before execution		
        subprocess.run(["rm","-f", input])
        subprocess.run(["rm","-f", processed])
		#Try to catch Errors in execution
        try:
            subprocess.run([TIPrintPath, input], check=True)
			#Blink faster to narrow down error source in case of failure
			#This realy is just there so that I'm able to blame others if it didn't work...
            pwm.ChangeFrequency(1)
            subprocess.run(["ffmpeg", "-i", input, "-vf", "hqx=4", processed], check=True)
            subprocess.run(["lp", processed], check=True)
        except subprocess.CalledProcessError:
            for i in range(0,4):
                GPIO.output(LED_Error,1)
                time.sleep(0.5)
                GPIO.output(LED_Error,0)
                time.sleep(1.5)
        pwm.ChangeFrequency(0.5)

except KeyboardInterrupt:
    GPIO.cleanup();
